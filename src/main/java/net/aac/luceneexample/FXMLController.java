package net.aac.luceneexample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class FXMLController implements Initializable {
    
    @FXML
    private Button btnIndex;
    
    @FXML
    private Button btnSearch;
    
    @FXML
    private javafx.scene.control.TextField txtSearch;
    
    @FXML
    private TextArea areaResult;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    private void handleBtnIndex(ActionEvent event) {
        indexDirectory();
    }
    
    @FXML
    private void handleBtnSearch(ActionEvent event) {
        if (!txtSearch.getText().isEmpty()) {
            search(txtSearch.getText());
        }
        else {
            areaResult.setText("You havent typed something");
        }
    }

    private void indexDirectory() {      
        try {  
            Path path = Paths.get(AppConstants.DIR_INDEXES); 
            Directory directory = FSDirectory.open(path);
            
            IndexWriterConfig config = new IndexWriterConfig(new SimpleAnalyzer());        
            IndexWriter indexWriter = new IndexWriter(directory, config);
            indexWriter.deleteAll();
            
            File dir = new File(AppConstants.DIR_FILES);
            
            for (File file : dir.listFiles()) {        
                Document doc = new Document();
                doc.add(new TextField("path", file.getName(), Store.YES));
                
                FileInputStream is = new FileInputStream(file);
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder stringBuffer = new StringBuilder();
                String line = null;
                
                while ((line = reader.readLine()) != null){
                  stringBuffer.append(line).append("\n");
                }
                
                reader.close();
                
                doc.add(new TextField("contents", stringBuffer.toString(), Store.YES));
                indexWriter.addDocument(doc);           
            }   
            
            indexWriter.close();           
            directory.close();
            
            areaResult.setText("Indexing success . . .");
        } catch (Exception e) {
            // TODO: handle exception
           areaResult.setText(e.getMessage());
        }                   
    }
    
    private void search(String text) {   
        try {   
            Path path = Paths.get(AppConstants.DIR_INDEXES);
            Directory directory = FSDirectory.open(path);       
            
            IndexReader indexReader =  DirectoryReader.open(directory);
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);
            
            QueryParser queryParser = new QueryParser("contents",  new StandardAnalyzer());  
            Query query = queryParser.parse(text);
            
            TopDocs topDocs = indexSearcher.search(query,10);
            StringBuilder msgResult = new StringBuilder();
            
            msgResult.append("Total hits: ").append(topDocs.totalHits).append("\n");
            msgResult.append("-----------------------------------------------------").append("\n");
            
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {           
                Document document = indexSearcher.doc(scoreDoc.doc);
                msgResult.append("Path: ").append(document.get("path")).append("\n");
                msgResult.append("Content: ").append(document.get("contents"));
                msgResult.append("\n");
            }
            
            areaResult.setText(msgResult.toString());
        } catch (IOException | ParseException e) {
            // TODO: handle exception
            areaResult.setText(e.getMessage());
        }               
    }
}
